package it.uniroma2.pjdm.esempilayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchLinear(View view) {
        Intent intent = new Intent(MainActivity.this, LinearActivity.class);
        startActivity(intent);
    }

    public void launchRelative(View view) {
        Intent intent = new Intent(MainActivity.this, RelativeActivity.class);
        startActivity(intent);
    }

    public void launchFrame(View view) {
        Intent intent = new Intent(MainActivity.this, FrameActivity.class);
        startActivity(intent);
    }
}
